#include <stdio.h>
#include <windows.h>
#include "../mailslot-ipc/mailslot-ipc.h"
#include "../mailslot-ipc/common-mailslots.h"

int main() {
	DWORD *pid = CreateMailslotAndReceiveOneValue(SenderSlotName, sizeof(DWORD), 1000);
	if (NULL == pid) {
		printf("Couldn't receive PID\n");
		exit(1);
	}
	
	printf("Received PID of the receiving process: %lu\n", *pid);

	HANDLE hProcReceiver = OpenProcess(PROCESS_ALL_ACCESS, FALSE, *pid);
	if (hProcReceiver == NULL) {
		printf("Couldn't open process: %d", GetLastError());
		exit(1);
	}

	const size_t MaxStringSize = 256;

	printf("Enter the message:\n");
	char *Message = malloc(MaxStringSize);
	
	scanf_s("%s", Message, MaxStringSize);

	printf("Message is %s\n", Message);
	
	LPVOID baseAddress = VirtualAllocEx(hProcReceiver, 0, MaxStringSize,
        MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (baseAddress == NULL) {
		printf("Couldn't allocate process memory: %d", GetLastError());
		exit(1);
	}

	if (!WriteProcessMemory(hProcReceiver, baseAddress, Message, MaxStringSize, 0)) {
		printf("Couldn't write to process memory: %d", GetLastError());
		exit(1);
	}

	printf("Sending address: %p\n", baseAddress);
	
	WriteValueToMailslot(ReceiverSlotName, &baseAddress, sizeof(LPVOID));

	CreateMailslotAndReceiveOneValue(SenderPingSlotName, 0, 0);

	if (!VirtualFreeEx(hProcReceiver, baseAddress, 0, MEM_RELEASE)) {
		printf("Couldn't free process memory: %d", GetLastError());
	}

	TerminateProcess(hProcReceiver,1);
	
	CloseHandle(hProcReceiver);
}
