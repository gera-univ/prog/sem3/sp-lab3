#pragma once

#include <windows.h>

LPCTSTR SenderSlotName = TEXT("\\\\.\\mailslot\\sender\\pid");
LPCTSTR ReceiverSlotName = TEXT("\\\\.\\mailslot\\receiver\\message_address");
LPCTSTR SenderPingSlotName = TEXT("\\\\.\\mailslot\\sender\\ping");