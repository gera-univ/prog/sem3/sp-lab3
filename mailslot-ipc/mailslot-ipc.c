#include "mailslot-ipc.h"

#include <stdio.h>

void* CreateMailslotAndReceiveOneValue(LPCTSTR SlotName, size_t ValueSize, DWORD SleepTime) {
	HANDLE hSlot = CreateMailslot(SlotName,
	                              ValueSize, // Only one value
	                              MAILSLOT_WAIT_FOREVER,
	                              (LPSECURITY_ATTRIBUTES)NULL);

	if (hSlot == INVALID_HANDLE_VALUE) {
		printf("CreateMailslot failed with %d\n", GetLastError());
		return NULL;
	}

	void* result = malloc(ValueSize);

	while (TRUE) {
		Sleep(SleepTime);

		DWORD cbMessage, cMessage;

		BOOL fResult = GetMailslotInfo(hSlot, // mailslot handle 
		                               (LPDWORD)NULL, // no maximum message size 
		                               &cbMessage, // size of next message 
		                               &cMessage, // number of messages 
		                               (LPDWORD)NULL); // no read time-out 

		if (!fResult) {
			printf("GetMailslotInfo failed with %d.\n", GetLastError());
			free(result);
			CloseHandle(hSlot);
			return NULL;
		}

		if (cbMessage == MAILSLOT_NO_MESSAGE) {
			printf("Waiting for a message...\n");
			continue;
		}

		DWORD cbRead;

		OVERLAPPED ov;
		HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, TEXT("ExampleSlot"));
		if (NULL == hEvent)
			return NULL;
		ov.Offset = 0;
		ov.OffsetHigh = 0;
		ov.hEvent = hEvent;

		fResult = ReadFile(hSlot,
		                   result,
		                   ValueSize,
		                   &cbRead,
		                   &ov);

		if (!fResult) {
			printf("ReadFile failed with %d.\n", GetLastError());
			return NULL;
		}

		CloseHandle(hEvent);
		
		return result;
	}
}

BOOL WriteValueToMailslot(LPCTSTR SlotName, void *value, size_t ValueSize) {
		HANDLE hFile = CreateFile(SlotName,
	                          GENERIC_WRITE,
	                          FILE_SHARE_READ,
	                          (LPSECURITY_ATTRIBUTES)NULL,
	                          OPEN_EXISTING,
	                          FILE_ATTRIBUTE_NORMAL,
	                          (HANDLE)NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		printf("CreateFile failed with %d.\n", GetLastError());
		return FALSE;
	}

	DWORD cbWritten;
	BOOL fResult = WriteFile(hFile,
	                         value,
	                         ValueSize,
	                         &cbWritten,
	                         (LPOVERLAPPED)NULL);
	if (!fResult) {
		printf("WriteFile failed with %d.\n", GetLastError());
		return FALSE;
	}

	return TRUE;
}
