#pragma once

#include <windows.h>

void* CreateMailslotAndReceiveOneValue(LPCTSTR SlotName, size_t ValueSize, DWORD SleepTime);

BOOL WriteValueToMailslot(LPCTSTR SlotName, void *value, size_t ValueSize);