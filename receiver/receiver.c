#include <stdio.h>
#include <windows.h>
#include "../mailslot-ipc/mailslot-ipc.h"
#include "../mailslot-ipc/common-mailslots.h"

int main() {
	DWORD pid = GetCurrentProcessId();

	printf("My pid is %lu\n", pid);
	
	if (!WriteValueToMailslot(SenderSlotName, &pid, sizeof(pid))) {
		printf("Couldn't write value\n");
		exit(1);
	}

	LPVOID *baseAddress = CreateMailslotAndReceiveOneValue(ReceiverSlotName, sizeof(LPVOID), 100);
	char *receivedString = (char *)*baseAddress;

	printf("Received address: %p\n", *baseAddress);
	printf("Received message: %s\n", receivedString);

	if (!WriteValueToMailslot(SenderPingSlotName, NULL, 0)) {
		printf("Couldn't write value\n");
		exit(1);
	}
	
	printf("Waiting to be terminated...\n");

	HANDLE hCurrentThread = GetCurrentThread();
	SuspendThread(hCurrentThread);
}
